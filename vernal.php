<?php
/*
plugin name: Vernal Creative Core
version: 0.1
description: Common vernal project functionality.
author: vernal creative
author uri: http://vernalcreative.com
plugin uri: http://bitbucket.org/cyberhobo/vernal
text domain: vernal
domain path: /languages
*/

namespace Vernal\Autoload;

require_once  dirname( __FILE__ ) . '/core/autoload.php';
\VernalCore_Autoload::register();

