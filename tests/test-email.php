<?php

class VernalCoreEmailTest extends WP_UnitTestCase {

	function test_email_defaults() {
		$email = new Vernal\Core\Email( array( 'to' => 'test@vernal.org' ) );
		$this->assertNotEmpty( $email->from_email );
		$this->assertNotEmpty( $email->subject );
		$this->assertEmpty( $email->message );
	}

	function test_email_to_user() {
		$user = $this->factory->user->create_and_get();
		$test_fields = array(
			'to' => $user->user_email,
			'from_name' => 'From Name',
			'from_email' => 'from@email.org',
			'subject' => 'Test Subject',
			'message' => 'Test Message',
			'reply_name' => 'Reply Name',
			'reply_email' => 'reply@email.org'
		);
		$email = new Vernal\Core\Email( $test_fields );

		$test = $this;
		$check_email = function( $args ) use ( $test, $test_fields ) {
			$test->assertEquals( $test_fields['to'], $args['to'] );
			$test->assertEquals( $test_fields['subject'], $args['subject'] );
			$test->assertEquals( $test_fields['message'], $args['message'] );
			$test->assertGreaterThanOrEqual( 2, count( $args['headers'] ) );
			foreach( $args['headers'] as $header ) {
				if ( 'from:' === substr( $header, 0, 5 ) )
					$from = $header;
				else if ( 'reply-to:' === substr( $header, 0, 9 ) )
					$reply = $header;
			}
			$test->assertNotEmpty( $from );
			$test->assertRegExp( '/' . $test_fields['from_email'] . '/', $from );
			$test->assertRegExp( '/' . $test_fields['from_name'] . '/', $from );
			$test->assertNotEmpty( $reply );
			$test->assertRegExp( '/' . $test_fields['reply_email'] . '/', $reply );
			$test->assertRegExp( '/' . $test_fields['reply_name'] . '/', $reply );
			return $args;
		};
		add_filter( 'wp_mail', $check_email );

		$email->send();
	}
}
