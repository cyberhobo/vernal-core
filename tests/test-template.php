<?php

class VernalCoreTemplateTest extends WP_UnitTestCase {

	function test_non_existant_template() {
		$template = new Vernal\Core\Template( 'fake.php' );
		$this->assertEmpty( $template->render( array() ) );
	}

	function test_fallback_template() {
		$template = new Vernal\Core\Template( 'vernal-test-template.php', dirname( __FILE__ ) );
		$data = array( 'message' => 'TEST ME' );
		$output = $template->render( $data, false );
		$this->assertNotEmpty( $output );
		$this->assertRegExp( '/' . $data['message'] . '/', $output, 'Message not found in output.' );
	}
}
