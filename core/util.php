<?php

namespace Vernal\Core;

class Util {

	/**
	 * If an URL is missing a protocol prefix, add a default.
	 * @param string $url
	 * @param string $default
	 * @return string URL with prefix.
	 */
	static function ensure_url_protocol( $url, $default = 'http://' ) {
		if ( $url and !preg_match( '/^.+:\/\//', $url ) )
			$url = $default . $url;
		return $url;
	}

	/**
	 * Translate a term taxonomy ID to a term object.
	 * @param $tt_id
	 * @return mixed
	 */
	static function get_term_by_tt_id( $tt_id ) {
		global $wpdb;
		$tt = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $wpdb->term_taxonomy WHERE term_taxonomy_id=%d", $tt_id ) );
		if ( $tt )
			return get_term( $tt->term_id, $tt->taxonomy );
		else
			return null;
	}
}

