<?php

class VernalCore_Autoload {

	protected function __construct() {
		$this->basedir = dirname( __FILE__ );
	}

	static function register() {
		$loader = new self();

		spl_autoload_register( array( $loader, 'autoload' ) );
	}

	function autoload( $class ) {

		if ( strpos( $class, 'Vernal\\Core\\' ) !== 0 )
			return;

		$path = str_replace( 'Vernal\\Core\\', '', $class );
		$path = str_replace( '_', '-', strtolower( $path ) );

		$file = sprintf( '%s/%s.php', $this->basedir, $path );

		if ( is_file( $file ) ) {
			require $file;
		}
	}
}
