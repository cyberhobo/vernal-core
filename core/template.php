<?php

namespace Vernal\Core;

class Template {
	private $template;

	function __construct( $names, $fallback_dir = '' ) {
		$this->template = locate_template( $names );
		if ( !$this->template and $fallback_dir ) {
			$fallback = null;
			foreach( (array) $names as $name ) {
				$fallback = path_join( $fallback_dir, $name );
				if ( is_readable( $fallback ) )
					break;
			}
			$this->template = $fallback;
		}
	}

	/**
	 * Render the template with an array of data in scope.
	 *
	 * @param array $data An array of data to provide to the template
	 * @param boolean $echo Whether to echo output, default true
	 * @return string Rendered output
	 */
	function render( $data, $echo = true ) {
		$output = '';
		if ( $this->template ) {
			extract( $data );
			if ( !$echo )
				ob_start();
			require( $this->template );
			if ( !$echo )
				$output = ob_get_clean();
		}
		return $output;
	}

}