<?php

namespace Vernal\Core;

class Breadcrumbs {

	static public function show( $args = '' ) {
		echo self::get( $args );
	}

	static function get( $args = '' ) {
		$default_args = array(
			'insert_page_id' => null,
			'include_home' => true,
			'include_terms' => '',
			'before' => '<p class="breadcrumbs">',
			'separator' => ' &gt; ',
			'after' => '</p>',
		);
		$args = wp_parse_args( $args, $default_args );
		/** @var $insert_page_id */
		/** @var $include_home */
		/** @var $include_terms */
		/** @var $before */
		/** @var $separator */
		/** @var $after */
		extract( $args );

		$crumbs = array();

		if ( $include_home )
			$crumbs[] = array( 'url' => home_url(), 'text' => 'Home' );

		if ( $insert_page_id ) {
			$leaf_object = get_queried_object();
			$queried_object = get_post( $insert_page_id );
		} else {
			$leaf_object = null;
			$queried_object = get_queried_object();
		}

		if ( isset( $queried_object->post_type ) ) {
			$ancestor_ids = array_reverse( get_ancestors( $queried_object->ID, $queried_object->post_type ) );
			foreach( $ancestor_ids as $ancestor_id ) {
				$ancestor = get_post( $ancestor_id );
				$crumbs[] = array(
					'url' => get_permalink( $ancestor->ID ),
					'text' => $ancestor->post_title
				);
			}
			$crumbs[] = array(
				'url' => get_permalink( $queried_object->ID ),
				'text' => $queried_object->post_title
			);

			if ( !is_array( $include_terms ) )
				$include_terms = array( $include_terms );
			$term_object = $leaf_object ?: $queried_object;
			foreach ( $include_terms as $taxonomy ) {
				$terms = get_the_terms( $term_object, $taxonomy );
				if ( $terms ) {
					foreach( $terms as $term ) {
						$crumbs[] = array(
							'url' => get_term_link( $term, $taxonomy ),
							'text' => $term->name,
						);
					}
				}
			}
		}

		if ( $leaf_object ) {
			if ( isset( $leaf_object->post_title ) )
				$crumbs[] = array( 'text' => $leaf_object->post_title  );
			else if ( isset( $leaf_object->name ) )
				$crumbs[] = array( 'text' => $leaf_object->name  );
			else if ( isset( $leaf_object->display_name ) )
				$crumbs[] = array( 'text' => $leaf_object->display_name  );
		}

		$assembled_crumbs = array();
		for( $i = 0; $i < count( $crumbs ) - 1; $i += 1 ) {
			$assembled_crumbs[] = '<a href="' . $crumbs[$i]['url'] . '">' .
				$crumbs[$i]['text'] . '</a>';
		}
		$leaf_crumb = array_pop( $crumbs );
		$assembled_crumbs[] = '<strong>' . $leaf_crumb['text'] . '</strong>';

		return $before . implode( $separator, $assembled_crumbs ) . $after;
	}
}

