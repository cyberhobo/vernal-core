<?php

namespace Vernal\Core;

class Email {
	public $to;
	public $subject;
	public $message;
	public $headers;
	public $from_name;
	public $from_email;
	public $reply_name;
	public $reply_email;
	public $content_type;

	static public function full_address( $email = null, $name = '' ) {
		if ( !$email )
			return self::default_from_email();

		if ( empty( $name ) )
			return $email;

		return $name . ' <' . $email . '>';
	}

	static public function default_from_email() {
		$server_name = isset( $_SERVER['SERVER_NAME'] ) ? $_SERVER['SERVER_NAME'] : gethostname();
		$domain = strtolower( $server_name );
		if ( substr( $domain, 0, 4 ) == 'www.' ) {
			$domain = substr( $domain, 4 );
		}

		return apply_filters( 'vernal_default_from_email', 'WordPress@' . $domain );
	}

	public function __construct( $values = '' ) {
		$defaults = array(
			'to' => '',
			'subject' => '[' . get_option( 'blogname' ) . '] ' . __( 'notification', 'Vernal' ),
			'message' => '',
			'headers' => array(),
			'from_name' => get_option( 'blogname' ),
			'from_email' =>  self::default_from_email(),
			'reply_name' => '',
			'reply_email' => '',
			'content_type' => 'text/html',
		);
		$values = wp_parse_args( $values, $defaults );
		foreach( $values as $name => $value ) {
			$this->$name = $value;
		}
	}

	public function send() {
		if ( !is_array( $this->headers ) )
			$this->headers = (array) $this->headers;

		$this->headers[] = 'from: ' . self::full_address( $this->from_email, $this->from_name );

		if ( !empty( $this->reply_email ) )
			$this->headers[] = 'reply-to: ' . self::full_address( $this->reply_email, $this->reply_name );

		// About changing content type: http://core.trac.wordpress.org/ticket/23578
		add_filter( 'wp_mail_content_type', array( $this, 'get_content_type' ) );
		$result = wp_mail( $this->to, $this->subject, $this->message, $this->headers );
		remove_filter( 'wp_mail_content_type', array( $this, 'get_content_type' ) );
		return $result;
	}

	public function get_content_type() {
		return $this->content_type;
	}
}